﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class LevitateBehavior : MonoBehaviour
{
    private const string levitateButton = "Levitate";
    private bool levitating = false;
    private Rigidbody rb;
    private GameManager gameManager;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (gameManager.levitateCheatOn)
        {
           
            if (!levitating)
            {
                rb.useGravity = false;
                var velocity = rb.velocity;
                velocity.y = 3;
                rb.velocity = velocity;
                levitating = true;
            }
        }
        else if (!gameManager.levitateCheatOn && levitating)
        {
            rb.useGravity = true;
            levitating = false;
        }
    }
}