﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollectibleBehavior : MonoBehaviour
{
    public AudioSource pickupSound;
    public string sceneName;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (pickupSound != null)
            {
                pickupSound.Play();
            }

            if (sceneName != null)
            {
                SceneManager.LoadScene(sceneName);
            }

            Destroy(this.gameObject);
        }
    }
}
