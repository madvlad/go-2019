﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheatMenuItemBehavior : MonoBehaviour
{
    public string cheatName;
    public GameObject button;

    private GameManager gameManager;
    private bool cheatEnabled;
    private bool cheatIsOn;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        cheatEnabled = gameManager.cheatsEnabled.ContainsKey(cheatName) ? gameManager.cheatsEnabled[cheatName] : false;

        if (!cheatEnabled)
        {
            this.GetComponent<Text>().text = "MEM READ ERR";
        }

        HandleColorChange();
    }

    public void ItemChanged()
    {
        HandleColorChange();
    }

    private void HandleColorChange()
    {
        cheatIsOn = gameManager.cheatsOn.ContainsKey(cheatName) ? gameManager.cheatsOn[cheatName] : false;

        if (cheatIsOn)
        {
            this.GetComponent<Text>().color = Color.green;
        } else
        {
            this.GetComponent<Text>().color = Color.red;
        }
    }

}
