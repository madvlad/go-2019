﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Camera;
using UnityEngine;

public class TalkBehavior : MonoBehaviour
{
    public GameObject TalkerHeadMesh;
    public GameObject DialogueView;
    public GameObject TextView;
    public GameObject xButtonGameObject;
    public GameObject UICamera;

    private GameObject shadowGameObject;

    private bool showingDialogue = false;
    private RigidbodyConstraints previousConstraints;

    private void Start()
    {
        this.shadowGameObject = GameObject.FindGameObjectWithTag("Player");
        this.UICamera = GameObject.FindGameObjectWithTag("UICamera");
    }

    private void FixedUpdate()
    {
        if (this.showingDialogue && Input.GetButtonDown("Jump") && this.TextView.GetComponent<AutoType>().finished)
        {
            this.UICamera.GetComponent<AudioListener>().enabled = false;
            this.shadowGameObject.SetActive(true);
            this.DialogueView.SetActive(false);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            this.TalkerHeadMesh.transform.LookAt(other.transform);
            this.xButtonGameObject.SetActive(true);

            if (Input.GetButtonDown("Jump") && !this.showingDialogue)
            {
                this.UICamera.GetComponent<AudioListener>().enabled = !this.showingDialogue;
                this.shadowGameObject.SetActive(this.showingDialogue);

                this.DialogueView.SetActive(!this.showingDialogue);
                this.showingDialogue = true;
            }
        }
   
    }

    private void OnTriggerExit(Collider other)
    {
        this.xButtonGameObject.SetActive(false);
        this.showingDialogue = false;
    }
}
