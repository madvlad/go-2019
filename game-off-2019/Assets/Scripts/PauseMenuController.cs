﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Camera;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour
{
    public string pauseButton;
    public GameObject pauseMenuUI;
    private bool gameIsPaused = false;
    private GameObject mainCamera;
    private int pixelSize = 5;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = GameObject.FindWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown(pauseButton))
        {
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Cursor.visible = false;
        Time.timeScale = 1f;
        gameIsPaused = false;
        mainCamera.GetComponent<UnityEngine.Camera>().pixelRect = new Rect(0, 0, Screen.width / pixelSize, Screen.height / pixelSize);
        mainCamera.GetComponent<CameraScreenGrab>().enabled = true;
        mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = true;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Cursor.visible = true;
        Time.timeScale = 0f;
        gameIsPaused = true;
        mainCamera.GetComponent<UnityEngine.Camera>().pixelRect = new Rect(0, 0, Screen.width, Screen.height);
        mainCamera.GetComponent<CameraScreenGrab>().enabled = false;
        mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
    }

    public void LoadMainMenu() {
        SceneManager.LoadScene("MainMenu"); // ideally we would use a constant variable but eh...
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
