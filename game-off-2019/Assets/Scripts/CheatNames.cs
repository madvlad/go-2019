﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class CheatNames
    {
        public static string LEVITATE = "levitate";
        public static string GIANT = "giant";
        public static string SHRINK = "shrink";
    }
}
