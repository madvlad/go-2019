﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject creditsMenu;

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void MainMenuToCredits()
    {
        this.gameObject.SetActive(false);
        creditsMenu.SetActive(true);
    }

    public void CreditsToMainMenu()
    {
        this.gameObject.SetActive(true);
        creditsMenu.SetActive(false);
    }
}
