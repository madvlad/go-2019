using Assets.Scripts.Camera;
using UnityEngine;
using UnityEngine.UI;

public class CheatMenuController : MonoBehaviour
{
    [Header("Sound")]
    public AudioSource music;
    public AudioClip menuOnSound;

    [Header("Menu Controls")]
    public string OpenMenuButton;

    [Header("Menu Objects")]
    public GameObject overlay;
    private int pixelSize = 5;
    private GameObject mainCamera;



    // Start is called before the first frame update
    void Start()
    {
        overlay.gameObject.SetActive(false);
        Cursor.visible = false;
        mainCamera = GameObject.FindWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown(OpenMenuButton))
        {
            ToggleMenu();
        }
    }

    public void ToggleMenu()
    {
        if (overlay.activeSelf)
        {
            music.UnPause();
            gameObject.GetComponent<AudioSource>().PlayOneShot(this.menuOnSound);
            overlay.gameObject.SetActive(false);
            Time.timeScale = 1;
            Cursor.visible = false;
            //mainCamera.GetComponent<UnityEngine.Camera>().pixelRect = new Rect(0, 0, Screen.width / pixelSize, Screen.height / pixelSize);
            mainCamera.GetComponent<CameraScreenGrab>().enabled = true;
            mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = true;
        }
        else
        {
            music.Pause();
            gameObject.GetComponent<AudioSource>().PlayOneShot(this.menuOnSound);
            overlay.gameObject.SetActive(true);
            Time.timeScale = 0;
            Cursor.visible = true;
            //mainCamera.GetComponent<UnityEngine.Camera>().pixelRect = new Rect(0, 0, Screen.width, Screen.height);
            mainCamera.GetComponent<CameraScreenGrab>().enabled = false;
            mainCamera.GetComponent<ThirdPersonOrbitCamBasic>().enabled = false;
        }
    }
}
