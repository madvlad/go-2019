﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResizeBehavior : MonoBehaviour
{

    private const string shrinkButton = "Shrink";
    private const string growButton = "Grow";

    private readonly Vector3 smallSize = new Vector3(.2f, .2f, .2f);
    private readonly Vector3 normalSize = new Vector3(1f, 1f, 1f);
    private readonly Vector3 largeSize = new Vector3(5f, 5f, 5f);

    private GameManager gameManager;


    // Start is called before the first frame update
    void Start()
    {
        this.gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.shrinkCheatOn)
        {
            transform.localScale = smallSize;
        }
        else if (gameManager.giantCheatOn)
        {
            transform.localScale = largeSize;
        }
        else
        {
            transform.localScale = normalSize;
        }
    }
}
