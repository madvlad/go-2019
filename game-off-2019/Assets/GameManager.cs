﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Dictionary<string, bool> cheatsEnabled = new Dictionary<string, bool>()
    {
        { CheatNames.LEVITATE, true },
        { CheatNames.SHRINK, false },
        { CheatNames.GIANT, false }
    };

    public Dictionary<string, bool> cheatsOn = new Dictionary<string, bool>()
    {
        { CheatNames.LEVITATE, false },
        { CheatNames.SHRINK, false },
        { CheatNames.GIANT, false }
    };

    public bool levitateEnabled = true;
    public bool shrinkEnabled = false;
    public bool giantEnabled = false;

    public bool levitateCheatOn = false;
    public bool shrinkCheatOn = false;
    public bool giantCheatOn = false;

    public void ToggleLevitate()
    {
        if (this.cheatsEnabled[CheatNames.LEVITATE])
        {
            this.levitateCheatOn = !this.levitateCheatOn;
            this.cheatsOn[CheatNames.LEVITATE] = this.levitateCheatOn;
        }
    }

    public void ToggleShrinkCheat()
    {
        if (this.cheatsEnabled[CheatNames.SHRINK])
        {
            this.shrinkCheatOn = !this.shrinkCheatOn;
            this.cheatsOn[CheatNames.SHRINK] = this.shrinkCheatOn;
        }
    }

    public void ToggleGiantCheat()
    {
        if (this.cheatsEnabled[CheatNames.GIANT])
        {
            this.giantCheatOn = !this.giantCheatOn;
            this.cheatsOn[CheatNames.GIANT] = this.giantCheatOn;
        }
    }
}
